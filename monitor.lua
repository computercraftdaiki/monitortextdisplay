shell.run("clear")
print("[V1.2] Monitor automatic & simple display by DaikiKaminari")
print("")

--- INIT ---
if not fs.exists("json") then
	shell.run("pastebin get 4nRg9CHU json")
end

if not fs.exists("objectJSON") then
	shell.run("pastebin get zqV8ihh0 objectJSON")
end
os.loadAPI("objectJSON")


--- VARIABLES ---
local objConf
local textFileName
local label
local textColor
local scale
local backgroundColor


--- UTIL FUNCTIONS ---
local function hasValue(tab, val)
    for index, value in pairs(tab) do
        if value == val then
            return true
        end
    end
    return false
end

local function hasIndex(tab, ind)
    for index, value in pairs(tab) do
        if index == ind then
            return true
        end
    end
    return false
end

--- FUNCTIONS ---
-- display saved text from a file
local function displayTextFromFile(fileName)
	local f = fs.open(fileName, "r")
	local line = f.readLine()
	while line ~= nil do
		print(line)
		line = f.readLine()
	end
	f.close()
end

-- save text a user enter into a file
local function getTextFromInput()
	print("")
	print("Ecrire le texte a afficher : ")
	local nbEnter = 0
	local textToSave = ""
	local line = "EMPTY"
	while nbEnter < 2 do
		line = io.read()
		textToSave = textToSave .. line .. "\n"
		if line == "" then
			nbEnter = nbEnter + 1
		else
			nbEnter = 0
		end
	end
	textToSave = string.sub(textToSave, 1, string.len(textToSave) - 2)
	return textToSave
end

-- display a text containe in a file coming back to line automatically
local function getTextFromFile(fileName)
	local f = fs.open(fileName, "r")
	local text = f.readAll()
	f.close()
	return text
end

-- save a text into a file
local function saveTextToFile(fileName, text)
	local h = fs.open(fileName, "w")
	h.write(text)
	h.close()
end

local colorsList = {["white"]=1, ["orange"]=2, ["magenta"]=4, ["lightBlue"]=8, ["yellow"]=16, ["lime"]=32, ["pink"]=64, ["gray"]=128, ["lightGray"]=256, ["cyan"]=512, ["purple"]=1024, ["blue"]=2048, ["brown"]=4096, ["green"]=8192, ["red"]=16384, ["black"]=32768}
local function selectTextColor()
	for col, val in pairs(colorsList) do
		print(col)
	end
	print("De quelle couleur doit etre le texte ?")
	colorText = io.read()
	if colorText == "" then
		colorText = "white"
	else
		while not hasIndex(colorsList, colorText) do
			print("Mauvaise couleur !")
			colorText = io.read()
		end
	end
	return colorText
end

local function selectTextScale()
	print("De quelle taille doit etre le texte (multiple de 0.5) ?")
	scale = tonumber(io.read())
	if (scale == nil) or (not (scale%0.5) == 0) then
		print("Mauvaise taille ! La taille sera celle par defaut : 1.5")
		scale = 1.5
	end
	return scale
end

local function selectBackgroundColor()
	for col, val in pairs(colorsList) do
		print(col)
	end
	print("De quelle couleur doit etre le fond ?")
	colorBackground = io.read()
	if colorBackground == "" then
		colorBackground = "black"
	else
		while not hasIndex(colorsList, colorBackground) do
			print("Mauvaise couleur !")
			colorBackground = io.read()
		end
	end
	return colorsList[colorBackground]
end

local function selectLabel()
	print("Donnez un nom a ce computer :")
	local label = io.read()
	if label == "" then
		label = "display_monitor"
	end
	return label
end

-- manage the monitor settings
local function config()
	--- VARIABLES ---
	-- case config file exist
	if fs.exists("config") then
		objConf = objectJSON.decodeFromFile("config")
		if objConf then
			print("ERROR : loading config file.")
			return 1
		end
		textFileName = objConf.textFileName
		textColor = objConf.textColor
		scale = objConf.scale
		backgroundColor = objConf.backgroundColor
	-- case config file doens't exist
	else
		textFileName = "textToDisplay"
		label = selectLabel()
		shell.run("label set " .. label)
		print("")
		textColor = selectTextColor()
		print("")
		backgroundColor = selectBackgroundColor()
		print("")
		scale = selectTextScale()
		print("")

		local t = {["mdr"] = 2}
		--local t = {textColor = textColor, backgroundColor = backgroundColor, scale = scale, textFileName = textFileName}
		objectJSON.encodeToFile("config", t)
		print("Config saved in \"config\" (.json)")
	end
	print("CURRENT LOADED SETTINGS :")
	print(textColor)
	print(scale)
	print(backgroundColor)
	monitorScreen = peripheral.find("monitor")
	if monitorScreen == nil then
		print("ERROR : no monitor detected.")
		return 1
	end

	--- SETTINGS ---
	monitorScreen.setTextColor(tonumber(colorsList[textColor]))
	monitorScreen.setTextScale(tonumber(scale))
	monitorScreen.setBackgroundColor(tonumber(backgroundColor))
	return 0
end

--- MAIN CALL ---
-- main function
local function main()
	if not config() then
		print("Program shutdown")
		return 1
	end

	local text
	if fs.exists(textFileName) then
		text = getTextFromFile(textFileName)
	else
		textToSave = getTextFromInput()
		saveTextToFile(textFileName, textToSave)
	end
	oldTerm = term.current()
	term.redirect(monitorScreen)
	monitorScreen.clear()
	displayTextFromFile(textFileName)
	term.redirect(oldTerm)
	print("Affichage termine sans probleme")
	return 0
end

if fs.exists("startup") then
	main()
else
	shell.run("pastebin get s3xcmqx8 startup")
	shell.run("clear")
	shell.run("startup")
end